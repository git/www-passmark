package WWW::Passmark;

use 5.014000;
use strict;
use warnings;
use parent qw/Exporter/;

our $VERSION = '0.001';
our @EXPORT_OK = qw/get_cpu_list get_gpu_list/;
our @EXPORT = @EXPORT_OK;

use WWW::Passmark::CPU;
use WWW::Passmark::GPU;

use HTML::TreeBuilder;
use LWP::UserAgent; # TreeBuilder->new_from_url uses this
use List::MoreUtils qw/natatime/;

use Data::Dumper qw/Dumper/;

our $CPU_URL = 'https://www.cpubenchmark.net/CPU_mega_page.html';
our $GPU_URL = 'https://www.videocardbenchmark.net/GPU_mega_page.html';

sub get_main_table {
	my ($url) = @_;
	my $tree = HTML::TreeBuilder->new_from_url($url);
	my $table = $tree->look_down(class => 'tablesorter');
	my $iter = natatime 2, $tree->find('tbody')->find('tr');

	my @results;
	while (my @items = $iter->()) {
		my ($item, $desc) = @items;
		push @results, [ map { $_->as_text } $item->find('td'), $desc->find('td') ]
	}

	@results
}

sub get_cpu_list {
	my @results = map { bless $_, 'WWW::Passmark::CPU' } get_main_table $CPU_URL;
	#bless \@results, 'WWW::Passmark::CPUList'
	\@results
}

sub get_gpu_list {
	my @results = map { bless $_, 'WWW::Passmark::GPU' } get_main_table $GPU_URL;
	#bless \@results, 'WWW::Passmark::GPUList'
	\@results
}

1;
__END__

=encoding utf-8

=head1 NAME

WWW::Passmark - look up CPU/GPU benchmark results

=head1 SYNOPSIS

  use WWW::Passmark;
  my $cpu_list = get_cpu_list;
  my $some_cpu = $cpu_list->[232];
  say $some_cpu->name; # AMD Ryzen 7 2700X
  say $some_cpu->socket; # AM4
  say $some_cpu->cpumark; # 17016
  say $some_cpu->test_date; # Apr 2018

  my $gpu_list = get_gpu_list;
  my $some_gpu = $gpu_list->[5];
  say $some_gpu->name; # Radeon R9 Fury
  say $some_gpu->g3dmark; # 9212

=head1 DESCRIPTION

This module is a scraper for Passmark software result websites. This
is the first version of the module, which only supports two of the
websites (cpubenchmark.net and videocardbenchmark.net) and offers few
features.

There are currently two functions, both exported by default. They are
B<get_cpu_list> and B<get_gpu_list>, which download the list of all
benchmark results for CPUs/GPUs and return an arrayref of objects of
type L<WWW::Passmark::CPU> or L<WWW::Passmark::GPU>. Each such object
represents one CPU/GPU, and contains its name and benchmark results.

=head1 SEE ALSO

L<https://www.cpubenchmark.net>, L<https://www.videocardbenchmark.net>

=head1 AUTHOR

Marius Gavrilescu, E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2018 by Marius Gavrilescu

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.24.1 or,
at your option, any later version of Perl 5 you may have available.


=cut

package WWW::Passmark::GPU;

use 5.014000;
use strict;
use warnings;

sub name { shift->[0] }
sub price { shift->[1] }
sub g3dmark { shift->[2] }
sub value { shift->[3] }
sub g2dmark { shift->[4] }
sub tdp { shift->[5] }
sub power_performance { shift->[6] }
sub test_date { shift->[7] }
sub category { shift->[8] }
sub extra_info { shift->[9] }

1;
__END__

=encoding utf-8

=head1 NAME

WWW::Passmark::GPU - benchmark results of a GPU

=head1 SYNOPSIS

  my $gpu = ...;
  say $gpu->name; # Radeon RX 580
  say $gpu->g3dmark; # 8376
  say $gpu->g2dmark; # 748
  say $gpu->test_date; # Apr 2017
  say $gpu->tdp; # 185

=head1 DESCRIPTION

An instance of this module represents benchmark results for some GPU.
Methods of this module return the various results, see source code for
a list of available methods.

=head1 AUTHOR

Marius Gavrilescu, E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2018 by Marius Gavrilescu

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.24.1 or,
at your option, any later version of Perl 5 you may have available.


=cut

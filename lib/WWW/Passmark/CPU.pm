package WWW::Passmark::CPU;

use 5.014000;
use strict;
use warnings;

sub name { shift->[0] }
sub price { shift->[1] }
sub cpumark { shift->[2] }
sub value { shift->[3] }
sub single_thread_mark { shift->[4] }
sub single_thread_value { shift->[5] }
sub tdp { shift->[6] }
sub power_performance { shift->[7] }
sub test_date { shift->[8] }
sub socket { shift->[9] }
sub category { shift->[10] }
sub extra_info { shift->[11] }

1;
__END__

=encoding utf-8

=head1 NAME

WWW::Passmark::CPU - benchmark results of a CPU

=head1 SYNOPSIS

  my $cpu = ...;
  say $cpu->name; # AMD FX-8350 Eight-Core
  say $cpu->cpumark; # 8950
  say $cpu->test_date; # Oct 2012
  say $cpu->tdp; # 125

=head1 DESCRIPTION

An instance of this module represents benchmark results for some CPU.
Methods of this module return the various results, see source code for
a list of available methods.

=head1 AUTHOR

Marius Gavrilescu, E<lt>marius@ieval.roE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2018 by Marius Gavrilescu

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.24.1 or,
at your option, any later version of Perl 5 you may have available.


=cut

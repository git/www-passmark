#!/usr/bin/perl
use strict;
use warnings;

use Test::RequiresInternet ('www.cpubenchmark.net' => 443, 'www.videocardbenchmark.net' => 443);
use Test::More tests => 10;
use List::Util qw/first/;

BEGIN { use_ok('WWW::Passmark') };

my $cpu_list = get_cpu_list;
my $gpu_list = get_gpu_list;

my $fx8350 = first { $_->name =~ /AMD FX-8350/i } @$cpu_list;
my $rx460 = first { $_->name eq 'Radeon RX 460' } @$gpu_list;

ok defined $fx8350, 'Found AMD FX-8350 in CPU list';
ok defined $rx460, 'Found Radeon RX 460 in GPU list';

cmp_ok $fx8350->cpumark, '>', 0, 'cpumark is positive';
cmp_ok $fx8350->single_thread_mark, '>', 0, 'single_thread_mark is positive';
is $fx8350->tdp, 125, 'TDP is correct';
is $fx8350->socket, 'AM3+', 'socket is correct';

cmp_ok $rx460->g3dmark, '>', 0, 'g3dmark is positive';
cmp_ok $rx460->g2dmark, '>', 0, 'g2dmark is positive';
is $rx460->tdp, 75, 'TDP is correct';
